from queue import Queue

from coalib.settings.Section import Section
from coalib.testing.LocalBearTestHelper import LocalBearTestHelper

from pyannotate_bear.PyAnnotateBear import PyAnnotateBear

valid_function = """
def gcd(a, b):
    # type: (int, int) -> int
    while b:
        a, b = b, a%b
    return a
"""

invalid_function = """
def gcd(a, b):
    while b:
        a, b = b, a%b
    return a
"""


class PyAnnotateBearTest(LocalBearTestHelper):

    def setUp(self):
        self.uut = PyAnnotateBear(Section('name'), Queue())

    def test_valid(self):
        self.check_validity(self.uut, valid_function.splitlines())
